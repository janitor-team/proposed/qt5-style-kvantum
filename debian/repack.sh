#!/bin/bash

# Kvantum has a strange tarball with all files in the
# Kvantum dir - we strip that directory

echo "Repackaging ..."
TMP=$(mktemp -d)
PKG="$(dpkg-parsechangelog|sed 's/^Source: //p;d')_$2+repack.orig"

TARGETPATH=$(realpath ..)

echo "Extracting tarball ..."
tar xf "$3" -C "$TMP"

pushd "$TMP"
KVANTUM=$(realpath Kvantum-*)

echo "Strip useless Kvantum subdir ..."
mv $KVANTUM/Kvantum/* $KVANTUM
rm -rf $KVANTUM/Kvantum

echo "Creating repack tarball ..."
tar cfvJ "$PKG.tar.xz" Kvantum-*
echo "Repackaged tarball $PKG.tar.xz created in $TMP ..."
popd

echo "Remove the linked orig.tar ..."
rm -f "$3"

echo "Copy the repacked tar to it's designated place ..."
cp "$TMP/$PKG.tar.xz" "$TARGETPATH"

